let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    bun
    ncurses
    rome
    nodejs_20
  ];
  shellHook = ''
    export ROME_BINARY=${pkgs.rome}/bin/rome
  '';
}