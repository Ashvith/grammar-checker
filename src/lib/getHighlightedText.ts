import {
  avoidAbusiveWords,
  avoidRedundantConstruct,
  avoidStartingWithConjunctions,
  checkDuplicateWordsInASentence,
  checkFirstWordOfSentence,
  checkIncorrectContractions,
  checkIncorrectPunctuationSpacing,
  checkUseOfAdverbs,
  checkUseOfLongSentence,
  getMarkupText,
  highlightInterjectionsWithoutPunctuations,
  highlightUseOfOxymoron,
  highlightWordiness,
  initializeGrammar,
  useConsistentApostrophe,
} from "./grammarCheckers";

export function getHighlightedText(text: string): {
  generatedText: string;
  grammarLogs: object;
} {
  const log = {};
  let doc = initializeGrammar(text);
  [doc, log["checkFirstWordOfSentence"]] = checkFirstWordOfSentence(doc);
  [doc, log["checkUseOfAdverbs"]] = checkUseOfAdverbs(doc);
  [doc, log["avoidAbusiveWords"]] = avoidAbusiveWords(doc);
  [doc, log["highlightUseOfOxymoron"]] = highlightUseOfOxymoron(doc);
  [doc, log["highlightWordiness"]] = highlightWordiness(doc);
  [doc, log["checkDuplicateWordsInASentence"]] =
    checkDuplicateWordsInASentence(doc);
  [doc, log["useConsistentApostrophe"]] = useConsistentApostrophe(doc);
  [doc, log["avoidStartingWithConjunctions"]] =
    avoidStartingWithConjunctions(doc);
  [doc, log["checkIncorrectContractions"]] = checkIncorrectContractions(doc);
  [doc, log["checkIncorrectPunctuationSpacing"]] =
    checkIncorrectPunctuationSpacing(doc);
  [doc, log["checkUseOfLongSentence"], log["checkUseOfVeryLongSentence"]] =
    checkUseOfLongSentence(doc);
  [doc, log["highlightInterjectionsWithoutPunctuations"]] =
    highlightInterjectionsWithoutPunctuations(doc);
  [doc, log["avoidRedundantConstruct"]] = avoidRedundantConstruct(doc);
  return { generatedText: getMarkupText(doc), grammarLogs: log };
}
