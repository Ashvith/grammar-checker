import winkNLP from "wink-nlp";
import type { Document, ItemEntity, ItemSentence, ItemToken } from "wink-nlp";
import model from "wink-eng-lite-web-model";
import abusiveWords from "./abusiveWords";
import oxymoronList from "./oxymoronList";
import wordyPhrases from "./wordyPhrases";

const nlp = winkNLP(model);
const its = nlp.its;

const patterns = [
  { name: "adverbSentence", patterns: ["[ADV]"] },
  { name: "oxymoron", patterns: oxymoronList() },
  { name: "abusiveWords", patterns: abusiveWords() },
  { name: "wordyPhrases", patterns: wordyPhrases() },
  { name: "curlyApostrophes", patterns: ["[‘|’]"] },
  { name: "redundantPhrases", patterns: ["[TIME] [ADP|] [DET|THE|] [TIME]"] },
];

nlp.learnCustomEntities(patterns);

function stringIsNotEmpty(collection: Document | ItemSentence) {
  return collection.tokens().length() !== 0;
}

export function initializeGrammar(text: string) {
  return nlp.readDoc(text);
}

export function checkFirstWordOfSentence(doc) {
  const description = "Lower case should not be used";
  const sentences = doc.sentences();
  let count = 0;

  if (stringIsNotEmpty(doc)) {
    sentences.each((sentence: ItemSentence) => {
      const tokens = sentence.tokens();

      if (
        tokens.out().length > 0 &&
        tokens.itemAt(0).out(its.type) === "word"
      ) {
        const firstWord = tokens.itemAt(0);

        if (
          firstWord.out(its.case) !== "titleCase" &&
          !(
            firstWord.out(its.case) === "upperCase" &&
            firstWord.out().length <= 1
          )
        ) {
          count += 1;
          firstWord.markup(
            '<span id="checkFirstWordOfSentence">',
            '</span>'
          );
        }
      }
    });
  }
  return [doc, { count, description }];
}

export function checkUseOfAdverbs(doc) {
  const description = "Adverbs found, try to reduce their use";
  const adverbSentence = doc
    .customEntities()
    .filter(
      (sentence: ItemSentence) => sentence.out(its.type) === "adverbSentence",
    );
  const count = adverbSentence.length();

  adverbSentence.each((token) => {
    token.markup('<span id="checkUseOfAdverbs">','</span>')
  });
  return [doc, { count, description }];
}

export function avoidAbusiveWords(doc) {
  const description = "Abusive words found - use them only if it fits the context";
  const filteredEntities = doc
    .customEntities()
    .filter((entity) => entity.out(its.type) === "abusiveWords");
  const count = filteredEntities.length();

  filteredEntities.each((entity) => {
    entity.markup(
      '<span id="avoidAbusiveWords">',
      '</span>'
    )
  });

  return [doc, { count, description }];
}

export function highlightUseOfOxymoron(doc) {
  const description = "Oxymorons detected - use them if it fits the context";
  const filteredEntities = doc
    .customEntities()
    .filter((entity) => entity.out(its.type) === "oxymoron");
  const count = filteredEntities.length();

  filteredEntities.each((entity) => {
    entity.markup(
      '<span id="highlightUseOfOxymoron">',
      '</span>'
    )
  });
  return [doc, { count, description }];
}

export function highlightWordiness(doc) {
  const description =
    "Wordy phrases have been found - avoid them";
  const filteredEntities = doc
    .customEntities()
    .filter((entity: ItemEntity) => entity.out(its.type) === "wordyPhrases");
  const count = filteredEntities.length();

  filteredEntities.each((entity: ItemEntity) => {
    entity.markup(
      '<span id="highlightWordiness">',
      '</span>'
    )
  });
  return [doc, { count, description }];
}

export function checkDuplicateWordsInASentence(doc) {
  const description = "Duplicate words detected - avoid their usage, or break the current sentence.";
  const sentences = doc.sentences();
  const tokens = doc.tokens();
  const punctuationCount = tokens
    .filter(
      (token: ItemToken) =>
        token.out() === "." || token.out() === "!" || token.out() === "?",
    )
    .out().length;
  if (sentences.length() === punctuationCount) {
    let count = 0;
    sentences.each((sentence) => {
      const trimmedSentence = sentence
        .tokens()
        .filter((token) => token.out(its.type) !== "tabCRLF");
      trimmedSentence.each((token) => {
        const index = token.index();
        if (
          index < sentence.tokens().length() - 2 &&
          token.out() === trimmedSentence.itemAt(index + 1).out()
        ) {
          count += 1;
          token.markup(
            '<span id="checkDuplicateWordsInASentence">',
            '</span>'
          );
          trimmedSentence.itemAt(index + 1)
                          .markup(
                            '<span id="checkDuplicateWordsInASentence">',
                            '</span>'
                          );
        }
      });
    });
    return [doc, { count, description }];
  }
  return [doc, { count: 0, description }];
}

export function useConsistentApostrophe(doc) {
  const description =
    "Apostrophes are not consistent - fix them, or stick with flat apostrophes only";
  const filteredEntities = doc
    .customEntities()
    .filter((entity) => entity.out(its.type) === "curlyApostrophes");
  const count = filteredEntities.length();

  filteredEntities.each((symbol) => {
    symbol.markup(
      '<span id="useConsistentApostrophe">',
      '</span>'
    );
  });
  return [doc, { count, description }];
}

export function avoidStartingWithConjunctions(doc) {
  const description = "Sentences cannot start with conjunctions";
  const sentences = doc.sentences();
  let count = 0;

  if (stringIsNotEmpty(doc)) {
    sentences.each((sentence: ItemSentence) => {
      if (
        stringIsNotEmpty(sentence) &&
        sentence.tokens().itemAt(0).out(its.type) === "word"
      ) {
        const firstWord = sentence
          .tokens()
          .filter((word) => word.out(its.type) === "word")
          .itemAt(0);
        if (
          firstWord.out(its.pos) === "SCONJ" ||
          firstWord.out(its.pos) === "CCONJ"
        ) {
          count += 1;
          firstWord.markup(
            '<span id="avoidStartingWithConjunctions">',
            '</span>'
          );
        }
      }
    });
  }
  return [doc, { count, description }];
}

export function checkIncorrectContractions(doc) {
  const description = "These contractions are incorrect!";
  const filteredTokens = doc
    .tokens()
    .filter((token) => token.out(its.type) !== "tabCRLF")
    .filter((token) => token.out(its.contractionFlag) === true);
  const contractionToken = filteredTokens.filter((token) => {
    const index = token.index();
    return index % 2 !== 0 && !token.out(its.value).includes("'");
  });

  contractionToken.each((token) => {
    const index = token.index();
    filteredTokens.itemAt(index * 2)
                  .markup(
                    '<span id="checkIncorrectContractions">',
                    '</span>'
                  );
    token.markup(
      '<span id="checkIncorrectContractions">',
      '</span>'
    );
  });

  const count = contractionToken.out().length;
  return [doc, { count, description }];
}

export function checkIncorrectPunctuationSpacing(doc) {
  const description = "These punctuations are incorrect!";
  const tokens = doc.tokens();
  const filteredTokens = tokens.filter(
    (token: ItemToken) => token.out(its.pos) === "PUNCT",
  );
  const incorrectToken = filteredTokens.filter((token) => {
    const index = token.index() - 1;
    return (
      (index < filteredTokens.out().length - 1 &&
        !(
          token.out(its.precedingSpaces) === "" &&
          (tokens.itemAt(token.index() + 1).out(its.precedingSpaces) === " " ||
            tokens.itemAt(token.index() + 1).out(its.pos) !== "PUNCT")
        )) ||
      (index === filteredTokens.out().length - 1 &&
        !(token.out(its.precedingSpaces) === ""))
    );
  });
  const count = incorrectToken.out().length;
  incorrectToken.each((token: ItemToken) => {
    token.markup(
      '<span id="checkIncorrectPunctuationSpacing">',
      '</span>'
    );
  });
  return [doc, { count, description }];
}

export function checkUseOfLongSentence(doc) {
  const longSentenceDescription =
    "These sentences are long - try to shorten the length!";
  const veryLongSentenceDescription =
    "These sentences are very long - try to shorten the length!";

  let longSentenceCount = 0;
  let veryLongSentenceCount = 0;

  const sentences = doc.sentences();

  sentences.each((sentence: ItemSentence) => {
    const wordCount = sentence
      .tokens()
      .filter((token: ItemToken) => token.out(its.type) === "word")
      .out().length;
    if (wordCount >= 15 && wordCount < 21) {
      longSentenceCount += 1;
      sentence.markup(
        '<span id="checkUseOfLongSentence">',
        '</span>'
      );
    } else if (wordCount >= 21) {
      veryLongSentenceCount += 1;
      sentence.markup(
        '<span id="checkUseOfVeryLongSentence">',
        '</span>'
      );
    }
  });
  return [
    doc,
    {
      count: longSentenceCount,
      description: longSentenceDescription,
    },
    {
      count: veryLongSentenceCount,
      description: veryLongSentenceDescription,
    },
  ];
}

export function highlightInterjectionsWithoutPunctuations(doc) {
  const description = `These sentences have incorrect interjections without proper punctuations! Use the following - "!", ".", "," OR "?"`;

  let count = 0;
  const tokens = doc.tokens();
  const sentences = doc.sentences();

  if (
    sentences.length() ===
    tokens
      .filter(
        (token: ItemToken) =>
          token.out() === "." || token.out() === "!" || token.out() === "?",
      )
      .out().length
  ) {
    tokens
      .filter((token) => {
        const index = token.index();
        return (
          token.out(its.pos) === "INTJ" &&
          !(
            tokens.itemAt(index + 1).out() === "?" ||
            tokens.itemAt(index + 1).out() === "!" ||
            tokens.itemAt(index + 1).out() === "," ||
            tokens.itemAt(index + 1).out() === "."
          )
        );
      })
      .each((token: ItemToken) => {
        count += 1;
        token.markup(
          '<span id="highlightInterjectionsWithoutPunctuations">',
          '</span>'
        );
      });
  }
  return [doc, { count, description }];
}

export function avoidRedundantConstruct(doc) {
  const description = 'Redundant constructs detected! Not necessarily wrong, but avoid them!';

  const filteredEntities = doc
    .customEntities()
    .filter(
      (entity: ItemEntity) => entity.out(its.type) === "redundantPhrases",
    );
  const count = filteredEntities.length();

  filteredEntities.each((entity: ItemEntity) =>
    entity.markup(
      '<span id="avoidRedundantConstruct">',
      '</span>'
    )
  );
  return [doc, { count, description }];
}

export function getMarkupText(doc: Document) {
  return doc.out(its.markedUpText);
}
